'use strict';

class v3 {

    constructor(x, y, z) {
        if(typeof x === 'object') {
            this.x = x.x;
            this.y = x.y;
            this.z = x.z;
        } else {
            this.x = typeof x !== 'undefined' ? x : .0;
            this.y = typeof y !== 'undefined' ? y : this.x;
            this.z = typeof z !== 'undefined' ? z : this.x;
        }
    }

    normalized = function () {
        let d = new v3().distance(this);

        return new v3(
            this.x / d,
            this.y / d,
            this.z / d
        );
    }

    distance = function (p) {
        return Math.sqrt(
            Math.pow(p.x - this.x, 2)
            + Math.pow(p.y - this.y, 2)
            + Math.pow(p.z - this.z, 2)
        );
    }

    vSubstracted = function (v) {
        return new v3(
            this.x - v.x,
            this.y - v.y,
            this.z - v.z
        );
    }

    vAdded = function (v) {
        return new v3(
            this.x + v.x,
            this.y + v.y,
            this.z + v.z
        );
    }

    added = function (a) {
        return new v3(
            this.x + a,
            this.y + a,
            this.z + a
        );
    }

    multiplied = function (m) {
        return new v3(
            this.x * m,
            this.y * m,
            this.z * m
        );
    }

    vMultiplied = function(v) {
        return new v3(
            this.x * v.x,
            this.y * v.y,
            this.z * v.z
        );
    }

    clamped = function (min, max) {
        return new v3(
            Math.min(max, Math.max(min, this.x)),
            Math.min(max, Math.max(min, this.y)),
            Math.min(max, Math.max(min, this.z))
        );
    }

    direction = function (p) {
        return new v3(
            p.x - this.x,
            p.y - this.y,
            p.z - this.z
        );
    }

    reverted = function () {
        return new v3(
            -this.x,
            -this.y,
            -this.z
        );
    }

    cross = function (v) {
        return new v3(
            this.y * v.z - this.z * v.y,
            this.z * v.x - this.x * v.z,
            this.x * v.y - this.y * v.x
        );
    }

    dot = function (v) {
        return this.x * v.x
            + this.y * v.y
            + this.z * v.z;
    }

    fromComponents = function (order) {
        let keys = order.split('');

        return new v3(
            this[keys[0]],
            this[keys[1]],
            this[keys[2]]
        );
    }

    getObject = function() {
        return {
            x: this.x,
            y: this.y,
            z: this.z
        };
    }
}