importScripts('vmath.js');

self.onmessage = function (e) {
    let result = [];
    let xMax = Math.min(e.data.x + e.data.bucketSize, e.data.w);
    let yMax = Math.min(e.data.y + e.data.bucketSize, e.data.h);
    for (let x = e.data.x; x < xMax; x += e.data.ss) {
        for (let y = e.data.y; y < yMax; y += e.data.ss) {
            // here some calculations to return the color per pixel
            let color = new v3(x / e.data.w, y / e.data.h, 0);
            result.push(
                {
                    x: x,
                    y: y,
                    color: color.getObject(),
                }
            );
        }
    }
    self.postMessage(result);
};